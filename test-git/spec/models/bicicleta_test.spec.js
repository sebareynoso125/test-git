var bicicleta = require('../../models/bicicleta')

beforeEach(() => {
    bicicleta.allBicis = [];
}); 

describe('bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(bicicleta.allBicis.length).toBe(0);
    });
});

describe('bicicleta.add', () => {
    it('agregamos una bicicleta', () => {
        expect(bicicleta.allBicis.length).toBe(0);
        
        var a = new bicicleta(1, 'rojo', 'urbana', [-32.8962, -68.8471]);
        
        bicicleta.add(a);

        expect(bicicleta.allBicis.length).toBe(1);
        expect(bicicleta.allBicis[0]).toBe(a);
    });
});

describe('bicicleta.findById', () => {
    it('debe devolver la bici con Id = 1', () =>{
        expect(bicicleta.allBicis.length).toBe(0);

        var aBici = new bicicleta(1, 'verde', 'urbana');
        var aBici2 = new bicicleta(2, 'blanca', 'carrera');

        bicicleta.add(aBici);
        bicicleta.add(aBici2);

        var targetBici = bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});

describe('bicileta.removeById', () => {
    it('bicicleta con id = 2 no debe estar', () => {
        expect(bicicleta.allBicis.length).toBe(0);

        var aBici = new bicicleta(1, 'verde', 'urbana');
        var aBici2 = new bicicleta(2, 'blanca', 'carrera');

        bicicleta.add(aBici);
        bicicleta.add(aBici2);

        bicicleta.removeById(2);
        
        expect(bicicleta.allBicis.length).toBe(1);
        expect(bicicleta.allBicis[0].id).toBe(1);
    });
});