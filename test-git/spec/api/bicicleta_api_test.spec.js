var bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('bicicleta API', () => {
    describe('GET BICIBLETA /', () =>{
        it('Status 200', () =>{
            expect(bicicleta.allBicis.length).toBe(0);

            var a = new bicicleta(1, 'rojo', 'urbana', [-32.8962, -68.8471]);
            bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICIBLETA /create', () => { 
        it('Status 200', (done) =>{
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(bicicleta.findById(10).color).toBe("rojo");
                    done();
            });
        });
    });

    describe('POST BICIBLETA /update', () => { 
        it('Status 200', (done) =>{
            var headers = {'content-type' : 'application/json'};
            var uBici = '{"id":5, "color": "verde", "modelo": "montaña", "lat": -34, "lng": -54}';
            
            var a = new bicicleta(5, 'verde', 'urbana', [-32.8962, -68.8471]);
            
            bicicleta.add(a);

            expect(bicicleta.findById(5).modelo).toBe("urbana");

            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/update',
                body: uBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(bicicleta.findById(5).modelo).toBe("montaña");
                    done();
            });
        });
    });

    describe('DELETE BICICLETA /delete', () => {
        it('Status 200', (done) =>{
            var headers = {'content-type' : 'application/json'};
            var uBici = '{"id":20}';
            var a = new bicicleta(20, 'verde', 'urbana', [-32.8962, -68.8471]);
            
            bicicleta.add(a);

            request.delete({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/delete',
                body: uBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(bicicleta.allBicis.length).toBe(0);
                    done();
            });
        });
    });
});

