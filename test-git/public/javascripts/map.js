var map = L.map('main_map').setView([-32.889, -68.8445], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//***** PUNTOS RANDOM EN EL MAPA */
//L.marker([-32.88969, -68.8647]).addTo(map);
//L.marker([-32.8768, -68.842]).addTo(map);
//L.marker([-32.8972, -68.8472]).addTo(map);
//L.marker([-32.9098, -68.8676]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})