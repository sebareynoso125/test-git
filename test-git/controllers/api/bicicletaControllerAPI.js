const { allBicis } = require('../../models/bicicleta');
var bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

//1° se crea el controller, luego se pasa a la ruta /routes/api/bicicletas.js
exports.bicicleta_update_get = function(req,res){

    var uBici = bicicleta.findById(req.body.id);
    
    res.status(200).json({
        bicicleta: uBici
    });
} 

exports.bicicleta_update_post = function(req,res){
    
    var uBici = bicicleta.findById(req.body.id);
    uBici.id = req.body.id;
    uBici.color = req.body.color;
    uBici.modelo = req.body.modelo;
    uBici.ubicacion = [req.body.lat, req.body.lng];
    
    res.status(200).json({
        bicicleta: bicicleta.allBicis
    });
} 


exports.bicicleta_delete = function(req, res){
    bicicleta.removeById(req.body.id);
    res.status(200).send();
}