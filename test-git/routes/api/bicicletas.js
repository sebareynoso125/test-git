var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create)
//2° se hace un get para obtener la bicicleta a actualizar y un post para guardar los cambios
router.get('/update', bicicletaController.bicicleta_update_get);
router.post('/update', bicicletaController.bicicleta_update_post);
router.delete('/delete', bicicletaController.bicicleta_delete)

module.exports = router;